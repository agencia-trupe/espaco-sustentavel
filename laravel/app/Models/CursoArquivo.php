<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CursoArquivo extends Model
{
    protected $table = 'cursos_arquivos';

    protected $guarded = ['id'];

    public function scopeCurso($query, $id)
    {
        return $query->where('curso_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
