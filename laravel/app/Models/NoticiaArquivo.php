<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticiaArquivo extends Model
{
    protected $table = 'noticias_arquivos';

    protected $guarded = ['id'];

    public function scopeNoticia($query, $id)
    {
        return $query->where('noticia_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
