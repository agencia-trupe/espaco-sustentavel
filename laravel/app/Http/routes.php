<?php

Route::get('/', ['as' => 'empresa', 'uses' => 'HomeController@empresa']);
Route::get('areas-de-atuacao', ['as' => 'atuacao', 'uses' => 'HomeController@atuacao']);
Route::get('cursos/{curso_slug?}', ['as' => 'cursos', 'uses' => 'HomeController@cursos']);
Route::get('pesquisa-e-desenvolvimento', ['as' => 'pesquisa', 'uses' => 'HomeController@pesquisa']);
Route::get('noticias/{noticia_slug?}', ['as' => 'noticias', 'uses' => 'HomeController@noticias']);
Route::get('contato', ['as' => 'contato', 'uses' => 'HomeController@contato']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'HomeController@contatoEnvio']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);

    Route::resource('noticias', 'NoticiasController');
    Route::resource('noticias.arquivos', 'NoticiasArquivosController');
    Route::resource('cursos', 'CursosController');
    Route::resource('cursos.arquivos', 'CursosArquivosController');
    Route::resource('contatos-recebidos', 'ContatosRecebidosController');
    Route::resource('usuarios', 'UsuariosController');

    Route::post('ckeditor-upload', 'HomeController@imageUpload');

    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
