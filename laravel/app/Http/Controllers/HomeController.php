<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\Curso;
use App\Models\Noticia;
use App\Models\ContatoRecebido;

class HomeController extends Controller
{

    public function empresa() { return view('frontend.empresa'); }
    public function atuacao() { return view('frontend.atuacao'); }

    public function cursos(Curso $curso)
    {
        if (empty($curso->id)) {
            $cursos = Curso::ordenados()->get();
            return view('frontend.cursos.index', compact('cursos'));
        } else {
            return view('frontend.cursos.show', compact('curso'));
        }
    }

    public function pesquisa() { return view('frontend.pesquisa'); }

    public function noticias(Noticia $noticia)
    {
        if (empty($noticia->id)) {
            $noticias = Noticia::ordenados()->get();
            return view('frontend.noticias.index', compact('noticias'));
        } else {
            return view('frontend.noticias.show', compact('noticia'));
        }
    }

    public function contato() { return view('frontend.contato'); }
    public function contatoEnvio(ContatosRecebidosRequest $request)
    {
        ContatoRecebido::create($request->all());

        \Mail::send('emails.contato', $request->all(), function($message) use ($request)
        {
            $message->from('noreply@espacosustentavel.com', $request->get('nome'));
            $message->to('espacosustentavel@espacosustentavel.com', config('site.name'))
                    ->subject('[CONTATO VIA SITE] '.config('site.name'))
                    ->replyTo($request->get('email'), $request->get('nome'));
        });

        return redirect()->back()->with('success', true);
    }

}
