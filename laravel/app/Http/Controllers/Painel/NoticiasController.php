<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\NoticiasRequest;

use App\Http\Controllers\Controller;

use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index()
    {
        $noticias = Noticia::ordenados()->get();

        return view('painel.noticias.index', compact('noticias'));
    }

    public function create()
    {
        return view('painel.noticias.create');
    }

    public function store(NoticiasRequest $request)
    {
        try {

            $input = $request->all();

            Noticia::create($input);
            return redirect()->route('painel.noticias.index')->with('success', 'Notícia adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar notícia: '.$e->getMessage()]);

        }
    }

    public function edit(Noticia $noticia)
    {
        return view('painel.noticias.edit', compact('noticia'));
    }

    public function update(NoticiasRequest $request, Noticia $noticia)
    {
        try {

            $input = $request->all();

            $noticia->update($input);
            return redirect()->route('painel.noticias.index')->with('success', 'Notícia alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar notícia: '.$e->getMessage()]);

        }
    }

    public function destroy(Noticia $noticia)
    {
        try {

            $noticia->delete();
            return redirect()->route('painel.noticias.index')->with('success', 'Notícia excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir noticia: '.$e->getMessage()]);

        }
    }
}
