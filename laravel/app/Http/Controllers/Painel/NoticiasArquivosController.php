<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\NoticiasArquivosRequest;

use App\Http\Controllers\Controller;

use App\Models\Noticia;
use App\Models\NoticiaArquivo;

class NoticiasArquivosController extends Controller
{
    public function index(Noticia $noticia)
    {
        $arquivos = NoticiaArquivo::noticia($noticia->id)->ordenados()->get();

        return view('painel.noticias.arquivos.index', compact('noticia', 'arquivos'));
    }

    public function create(Noticia $noticia)
    {
        return view('painel.noticias.arquivos.create', compact('noticia'));
    }

    public function uploadArquivo($file) {
        $filePath = 'assets/arquivos/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function store(Noticia $noticia, NoticiasArquivosRequest $request)
    {
        try {
            $input = $request->all();
            $input['arquivo'] = $this->uploadArquivo($input['arquivo']);

            $noticia->arquivos()->create($input);

            return redirect()->route('painel.noticias.arquivos.index', $noticia)->with('success', 'Arquivo adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar arquivo: '.$e->getMessage()]);

        }
    }

    public function edit(Noticia $noticia, NoticiaArquivo $arquivo)
    {
        return view('painel.noticias.arquivos.edit', compact('noticia', 'arquivo'));
    }

    public function update(Noticia $noticia, NoticiaArquivo $arquivo, NoticiasArquivosRequest $request)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            } else {
                unset($input['arquivo']);
            }

            $arquivo->update($input);

            return redirect()->route('painel.noticias.arquivos.index', $noticia)->with('success', 'Arquivo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar arquivo: '.$e->getMessage()]);

        }
    }

    public function destroy(Noticia $noticia, NoticiaArquivo $arquivo)
    {
        try {

            $arquivo->delete();
            return redirect()->route('painel.noticias.arquivos.index', $noticia)
                             ->with('success', 'Arquivo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir arquivo: '.$e->getMessage()]);

        }
    }
}
