<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\CursosRequest;

use App\Http\Controllers\Controller;

use App\Models\Curso;

class CursosController extends Controller
{
    public function index()
    {
        $cursos = Curso::ordenados()->get();

        return view('painel.cursos.index', compact('cursos'));
    }

    public function create()
    {
        return view('painel.cursos.create');
    }

    public function store(CursosRequest $request)
    {
        try {

            $input = $request->all();

            Curso::create($input);
            return redirect()->route('painel.cursos.index')->with('success', 'Curso adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar curso: '.$e->getMessage()]);

        }
    }

    public function edit(Curso $curso)
    {
        return view('painel.cursos.edit', compact('curso'));
    }

    public function update(CursosRequest $request, Curso $curso)
    {
        try {

            $input = $request->all();

            $curso->update($input);
            return redirect()->route('painel.cursos.index')->with('success', 'Curso alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar curso: '.$e->getMessage()]);

        }
    }

    public function destroy(Curso $curso)
    {
        try {

            $curso->delete();
            return redirect()->route('painel.cursos.index')->with('success', 'Curso excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir curso: '.$e->getMessage()]);

        }
    }
}
