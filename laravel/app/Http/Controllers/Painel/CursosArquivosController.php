<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\CursosArquivosRequest;

use App\Http\Controllers\Controller;

use App\Models\Curso;
use App\Models\CursoArquivo;

class CursosArquivosController extends Controller
{
    public function index(Curso $curso)
    {
        $arquivos = CursoArquivo::curso($curso->id)->ordenados()->get();

        return view('painel.cursos.arquivos.index', compact('curso', 'arquivos'));
    }

    public function create(Curso $curso)
    {
        return view('painel.cursos.arquivos.create', compact('curso'));
    }

    public function uploadArquivo($file) {
        $filePath = 'assets/arquivos/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function store(Curso $curso, CursosArquivosRequest $request)
    {
        try {
            $input = $request->all();
            $input['arquivo'] = $this->uploadArquivo($input['arquivo']);

            $curso->arquivos()->create($input);

            return redirect()->route('painel.cursos.arquivos.index', $curso)->with('success', 'Arquivo adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar arquivo: '.$e->getMessage()]);

        }
    }

    public function edit(Curso $curso, CursoArquivo $arquivo)
    {
        return view('painel.cursos.arquivos.edit', compact('curso', 'arquivo'));
    }

    public function update(Curso $curso, CursoArquivo $arquivo, CursosArquivosRequest $request)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            } else {
                unset($input['arquivo']);
            }

            $arquivo->update($input);

            return redirect()->route('painel.cursos.arquivos.index', $curso)->with('success', 'Arquivo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar arquivo: '.$e->getMessage()]);

        }
    }

    public function destroy(Curso $curso, CursoArquivo $arquivo)
    {
        try {

            $arquivo->delete();
            return redirect()->route('painel.cursos.arquivos.index', $curso)
                             ->with('success', 'Arquivo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir arquivo: '.$e->getMessage()]);

        }
    }
}
