<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('noticias', 'App\Models\Noticia');
        $router->model('cursos', 'App\Models\Curso');

        $router->bind('arquivos', function($id, $route, $model = null) {
            if ($route->hasParameter('noticias')) {
                $model = \App\Models\NoticiaArquivo::find($id);
            } elseif ($route->hasParameter('cursos')) {
                $model = \App\Models\CursoArquivo::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->model('contatos-recebidos', 'App\Models\ContatoRecebido');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('noticia_slug', function($value) {
            return \App\Models\Noticia::slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('curso_slug', function($value) {
            return \App\Models\Curso::slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
