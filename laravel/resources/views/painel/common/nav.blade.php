<ul class="nav navbar-nav">
    <li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.noticias.index') }}">Notícias</a>
    </li>

    <li @if(str_is('painel.cursos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cursos.index') }}">Cursos</a>
    </li>

    <li @if(str_is('painel.contatos-recebidos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contatos-recebidos.index') }}">
            Contatos Recebidos
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
        </a>
    </li>
</ul>
