@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Notícia</h2>
    </legend>

    {!! Form::open(['route' => 'painel.noticias.store']) !!}

        @include('painel.noticias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
