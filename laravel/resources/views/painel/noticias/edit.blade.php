@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Notícia</h2>
    </legend>

    {!! Form::model($noticia, [
        'route'  => ['painel.noticias.update', $noticia->id],
        'method' => 'patch'])
    !!}

    @include('painel.noticias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
