@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Notícias
            <a href="{{ route('painel.noticias.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Notícia</a>
        </h2>
    </legend>

    @if(!count($noticias))
    <div class="alert alert-warning" role="alert">Nenhum notícia cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Título</th>
                <th>Arquivos</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($noticias as $noticia)
            <tr class="tr-row">
                <td>{{ $noticia->data }}</td>
                <td>{{ $noticia->titulo }}</td>
                <td><a href="{{ route('painel.noticias.arquivos.index', $noticia) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-file" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.noticias.destroy', $noticia->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.noticias.edit', $noticia->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
