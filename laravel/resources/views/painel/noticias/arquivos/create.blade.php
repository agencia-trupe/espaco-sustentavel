@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Notícias / {{ $noticia->titulo }} /</small> Adicionar Arquivo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.noticias.arquivos.store', $noticia], 'files' => true]) !!}

        @include('painel.noticias.arquivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
