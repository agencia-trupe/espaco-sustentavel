@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Notícias / {{ $noticia->titulo }} /</small> Editar Arquivo</h2>
    </legend>

    {!! Form::model($arquivo, [
        'route'  => ['painel.noticias.arquivos.update', $noticia, $arquivo],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.noticias.arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
