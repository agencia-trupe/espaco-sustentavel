@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.noticias.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Notícias
    </a>

    <legend>
        <h2>
            <small>Notícias / {{ $noticia->titulo }} /</small> Arquivos
            <a href="{{ route('painel.noticias.arquivos.create', $noticia) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Arquivo</a>
        </h2>
    </legend>

    @if(!count($arquivos))
    <div class="alert alert-warning" role="alert">Nenhum arquivo cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="noticias_arquivos">
        <thead>
            <tr>
                <th>Ordem</th>
                <th>Arquivo</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($arquivos as $arquivo)
            <tr class="tr-row" id="{{ $arquivo->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><a href="{{ url('assets/arquivos/'.$arquivo->arquivo) }}">{{ $arquivo->titulo }}</a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.noticias.arquivos.destroy', $noticia, $arquivo],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.noticias.arquivos.edit', [$noticia, $arquivo]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
