@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cursos / {{ $curso->titulo }} /</small> Editar Arquivo</h2>
    </legend>

    {!! Form::model($arquivo, [
        'route'  => ['painel.cursos.arquivos.update', $curso, $arquivo],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cursos.arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
