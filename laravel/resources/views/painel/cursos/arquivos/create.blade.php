@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cursos / {{ $curso->titulo }} /</small> Adicionar Arquivo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.cursos.arquivos.store', $curso], 'files' => true]) !!}

        @include('painel.cursos.arquivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
