@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Curso</h2>
    </legend>

    {!! Form::model($curso, [
        'route'  => ['painel.cursos.update', $curso->id],
        'method' => 'patch'])
    !!}

    @include('painel.cursos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
