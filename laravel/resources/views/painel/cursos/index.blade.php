@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Cursos
            <a href="{{ route('painel.cursos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Curso</a>
        </h2>
    </legend>

    @if(!count($cursos))
    <div class="alert alert-warning" role="alert">Nenhum curso cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Título</th>
                <th>Arquivos</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($cursos as $curso)
            <tr class="tr-row">
                <td>{{ $curso->data }}</td>
                <td>{{ $curso->titulo }}</td>
                <td><a href="{{ route('painel.cursos.arquivos.index', $curso) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-file" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.cursos.destroy', $curso->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.cursos.edit', $curso->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
