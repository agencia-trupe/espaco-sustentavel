@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Curso</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cursos.store']) !!}

        @include('painel.cursos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
