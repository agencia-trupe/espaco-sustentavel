@extends('frontend.common.template')

@section('content')

    <p class="tit16">{{ $noticia->titulo }}</p>
    {!! $noticia->texto !!}

    @if(count($noticia->arquivos))
    <div class="arquivos">
        @foreach($noticia->arquivos as $arquivo)
            <p>» <a href="{{ url('assets/arquivos/'.$arquivo->arquivo) }}" target="_blank">{{ $arquivo->titulo }}</a></p>
        @endforeach
    </div>
    @endif

    <p class="tit16_2"><a href="{{ route('noticias') }}" class="link_voltar">voltar</a></p>

@endsection
