@extends('frontend.common.template')

@section('content')

    <table width="500" border="0" cellspacing="0" cellpadding="0">
        @foreach($noticias as $noticia)
        <tr>
            <td class="pad5">
                <a href="{{ route('noticias', $noticia->slug) }}" class="link_noticia">{{ $noticia->titulo }}</a>
            </td>
        </tr>
        <tr><td class="linha_traco_h"><img src="assets/_img/spacer.gif" width="1" height="1" /></td></tr>
        @endforeach
    </table>

@endsection
