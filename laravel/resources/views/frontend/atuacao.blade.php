@extends('frontend.common.template')

@section('content')

    <p>
        <span class="tx_12">Consultoria para minimizar o consumo de energia relacionado a projetos de: </span>
        <br />
        • Arquitetura<br />
        • Envoltória (especificação de vidros e materiais de construção)<br />
        • Sistemas de condicionamento de ar e Ventilação<br />
        • Luminotécnica<br />
        • Automação<br />
        • Aquecimento de água
    </p>
    <p>
        <span class="tx_12">Análise de viabilidade de implantação de sistemas de:</span>
        <br />
        • Iluminação natural<br />
        • Ventilação Natural<br />
        • Sistemas híbridos de ventilação natural e ar condicionado<br />
        • Energia renovável
    </p>
    <p>
        <span class="tx_12">Análise tarifária de energia</span>
        <br />
        Simulação computacional do desempenho térmico e energético de edificações utilizando os softwares EnergyPlus, Window5, Optics, entre outros.
    </p>
    <p>
        Consultoria e simulação energética para certificações LEED (Leadership in Energy and Environmental Design)
        <br />
    </p>

@endsection
