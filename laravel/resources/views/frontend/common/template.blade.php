<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url() }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">

    <title>{{ config('site.title') }}</title>

    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr>
            <td>
                <table border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3">
                            <table border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="240" class="pad_5_B">
                                        <img src="assets/_img/logo_espaco_sutentavel.jpg" width="179" height="52" />
                                    </td>
                                    <td width="450">
                                        <table border="0" align="right" cellpadding="0" cellspacing="0">
                                            <tr><td height="20" class="tx_12">+55 11 2645<span class="tx_11">a</span>2077</td></tr>
                                            <tr><td height="20" class="tx_12">+55 11 98164<span class="tx_11">a</span>1307</td></tr>
                                        </table>
                                    </td>
                                    <td width="50" style="text-align:right;padding-right:5px">
                                        <a href="https://www.facebook.com/EspacoSustentavel" target="_blank" class="facebook">facebook</a>
                                        <a href="https://br.linkedin.com/in/espa%C3%A7o-sustent%C3%A1vel-thais-inatomi-1433852b" target="_blank" class="linkedin">linkedin</a>
                                    </td>
                                </tr>
                                <tr><td height="40" colspan="3" class="linha_h">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="240" valign="top">
                            <table width="240" border="0" cellspacing="0" cellpadding="0">
                                <tr><td valign="top" class="pad_25_B"><a href="{{ route('empresa') }}" class="link_menu">A Empresa</a></td></tr>
                                <tr><td valign="top" class="pad_25_B"><a href="{{ route('atuacao') }}" class="link_menu">&Aacute;reas de Atua&ccedil;&atilde;o</a></td></tr>
                                <tr><td valign="top" class="pad_25_B"><a href="{{ route('cursos') }}" class="link_menu">Cursos</a></td></tr>
                                <tr><td valign="top" class="pad_25_B"><a href="{{ route('pesquisa') }}" class="link_menu">Pesquisa e <br />Desenvolvimento</a></td> </tr>
                                <tr><td valign="top" class="pad_25_B"><a href="{{ route('noticias') }}" class="link_menu">Not&iacute;cias</a></td></tr>
                                <tr><td valign="top" class="pad_25_B"><a href="{{ route('contato') }}" class="link_menu">Contato</a></td></tr>
                            </table>
                        </td>
                        <td width="500" align="left" valign="top">@yield('content')</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br /><p>&nbsp;</p>
                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="770" height="65">
                    <param name="movie" value="assets/flash/rodape.swf">
                    <param name="quality" value="high">
                    <param name="wmode" value="opaque">
                    <embed src="assets/flash/rodape.swf" quality="high" wmode="opaque" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="770" height="65"></embed>
                </object>
            </td>
        </tr>
    </table>

    <table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" valign="bottom" class="pad5">&nbsp;</td>
        </tr>
    </table>
</body>
</html>
