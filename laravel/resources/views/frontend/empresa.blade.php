@extends('frontend.common.template')

@section('content')

    <p><img src="assets/_img/arvore.jpg" width="500" height="129" /></p>
    <p>A<span class="tx_14"> Espaço Sustentável</span> é uma empresa de consultoria criada para auxiliar seus clientes a implantar ou aprimorar a eficiência energética em suas edificações, conciliando seus projetos aos conceitos de desenvolvimento sustentável.</p>
    <p>Auxiliamos projetistas e empreendedores na tomada de decisões, tendo como objetivo a redução do consumo e custo com energia, buscando sempre as melhores soluções técnicas, ambientais e econômicas para edificações novas ou existentes (retrofits).</p>
    <p>Nossa experiência abrange obras de edifícios comerciais e residenciais, shopping centers, bancos, fábricas, galpões, centros de distribuição, restaurantes, fast-foods e hospitais.</p>

@endsection
