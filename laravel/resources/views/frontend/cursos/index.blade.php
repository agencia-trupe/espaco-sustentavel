@extends('frontend.common.template')

@section('content')

    <table width="500" border="0" cellspacing="0" cellpadding="0">
        @foreach($cursos as $curso)
        <tr>
            <td class="pad5">
                <a href="{{ route('cursos', $curso->slug) }}" class="link_noticia">{{ $curso->titulo }}</a>
            </td>
        </tr>
        <tr><td class="linha_traco_h"><img src="assets/_img/spacer.gif" width="1" height="1" /></td></tr>
        @endforeach
    </table>

@endsection
