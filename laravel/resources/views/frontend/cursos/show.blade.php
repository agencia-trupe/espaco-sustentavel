@extends('frontend.common.template')

@section('content')

    <p class="tit16">{{ $curso->titulo }}</p>
    {!! $curso->texto !!}

    @if(count($curso->arquivos))
    <div class="arquivos">
        @foreach($curso->arquivos as $arquivo)
            <p>» <a href="{{ url('assets/arquivos/'.$arquivo->arquivo) }}" target="_blank">{{ $arquivo->titulo }}</a></p>
        @endforeach
    </div>
    @endif

    <p class="tit16_2"><a href="{{ route('cursos') }}" class="link_voltar">voltar</a></p>

@endsection
