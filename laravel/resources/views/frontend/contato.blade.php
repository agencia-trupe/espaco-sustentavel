@extends('frontend.common.template')

@section('content')

    <form id="form1" name="form1" method="post" action="{{ route('contato.envio') }}">
        {!! csrf_field() !!}

        @if(session('success'))
        <div class="contato-response contato-response-success">
            <p>Agradecemos por sua mensagem!</p>
            <p>Em breve entraremos em contato.</p>
        </div>
        @elseif (count($errors) > 0)
        <div class="contato-response">
            @foreach($errors->all() as $error)
            <p>{!! $error !!}</p>
            @endforeach
        </div>
        @endif

        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="65" class="pad5">Nome:</td>
                <td><input name="nome" type="text" class="imput" id="Nome" value="{{ old('nome') }}" /></td>
            </tr>
            <tr>
                <td class="pad5">Empresa:</td>
                <td><input name="empresa" type="text" class="imput" id="empresa" value="{{ old('empresa') }}" /></td>
            </tr>
            <tr>
                <td class="pad5">E-mail:</td>
                <td><input name="email" type="text" class="imput" id="E-mail" value="{{ old('email') }}" /></td>
            </tr>
            <tr>
                <td class="pad5">Telefone:</td>
                <td><input name="telefone" type="text" class="imput3" id="telefone" value="{{ old('telefone') }}" /></td>
            </tr>
            <tr>
                <td valign="top" class="pad5">Mensagem:</td>
                <td><textarea name="mensagem" cols="45" rows="5" class="imput4" id="Mensagem">{{ old('mensagem') }}</textarea></td>
            </tr>
            <tr>
                <td align="right" class="pad5">&nbsp;</td>
                <td align="right"><input type="submit" class="bt1" id="button" value="Enviar" /></td>
            </tr>
        </table>
    </form>

    <p class="pad5">
        Av. Lacerda Franco, 570, s.104<br />
        01536-000 •São Paulo - SP • Brasil<br />
        <a href="mailto:espacosustentavel@espacosustentavel.com" class="link_texto2">
            espacosustentavel@espacosustentavel.com
        </a>
    </p>

@endsection
