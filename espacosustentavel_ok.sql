-- phpMyAdmin SQL Dump
-- version 4.4.14.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2015 at 07:17 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.6.10-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `espacosustentavel`
--

-- --------------------------------------------------------

--
-- Table structure for table `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(10) unsigned NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cursos_arquivos`
--

CREATE TABLE IF NOT EXISTS `cursos_arquivos` (
  `id` int(10) unsigned NOT NULL,
  `curso_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_10_132830_create_contatos_recebidos_table', 1),
('2015_11_10_132847_create_noticias_table', 1),
('2015_11_10_132905_create_noticias_arquivos_table', 1),
('2015_11_10_132921_create_cursos_table', 1),
('2015_11_10_132932_create_cursos_arquivos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(10) unsigned NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `data`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, '2015-11-10', 'Curitiba Office Park é LEED Silver!', 'curitiba-office-park-e-leed-silver', '<p><span style="font-family: Arial; font-size: small;">Empreendimento atinge 19% de redução do consumo de energia em relação à ASHRAE 90.1-2004</span></p><p><font face="Arial" size="2"><a href="http://www.gazetadopovo.com.br/imobiliario/conteudo.phtml?id=1161386&amp;tit=Curitiba-entra-na-era-dos-edificios-sustentaveis">Clique aqui e veja mais informações.</a></font>&nbsp;</p>', '2015-11-10 19:01:52', '2015-11-10 19:01:52'),
(2, '2015-11-10', 'O Novo Auditório do Edifício Sede da Odebrecht recebe Certificação LEED Silver!', 'o-novo-auditorio-do-edificio-sede-da-odebrecht-recebe-certificacao-leed-silver', '<p></p><p class="MsoNormal"></p><p class="MsoNormal"></p><p class="MsoNormal"><font size="2" face="Arial">A edificação alcançou eficiência energética de 21% em relação ao edifício de referência estabelecido pela norma ASHRAE 90.1 - 2004!</font></p>', '2015-11-10 19:02:07', '2015-11-10 19:02:07'),
(3, '2015-11-10', 'Centro de Distribuição da Iveco e Case New Holland recebe Certificação LEED Gold!', 'centro-de-distribuicao-da-iveco-e-case-new-holland-recebe-certificacao-leed-gold', '<span style="font-family: Arial; font-size: small;">O&nbsp;empreendimento atingiu eficiência energética de 46% e 40% nos seus dois galpões, conforme estabelecido pela norma ASHRAE 90.1-2004.</span><div><span style="font-family: Arial; font-size: small;"><br></span></div><div><span style="font-family: Arial; font-size: small;"><a href="http://www.ivecodiretto.com.br/PAGINA/620/CENTRO-DE-DISTRIBUI--199---195-O-DA-IVECO-E-CASE-NEW-HOLLAND-RECEBE-CERTIFICA--199---195-O-GREEN-BUILDING.aspx">Clique e veja mais informações.</a>&nbsp;</span></div>', '2015-11-10 19:02:18', '2015-11-10 19:02:18'),
(4, '2015-11-10', 'Unidade Alphaville do Fleury Medicina e Saúde recebe certificação LEED Gold!', 'unidade-alphaville-do-fleury-medicina-e-saude-recebe-certificacao-leed-gold', '<p class="MsoNormal" style="margin-bottom: 0.0001pt;"></p><p class="MsoNormal" style="margin-bottom: 0.0001pt;"><font face="Arial" size="2"><a href="http://www.revistainfra.com.br/portal/Textos/?Slider/13383/Fleury-Alphaville-%C3%A9-LEED-Ouro-">Veja a reportagem! Clique aqui.</a></font></p>', '2015-11-10 19:02:34', '2015-11-10 19:02:34'),
(5, '2015-11-10', 'Edifício Atrium Faria Lima recebe Certificação LEED!', 'edificio-atrium-faria-lima-recebe-certificacao-leed', '<p><font face="Arial"><a href="http://www.construirsustentavel.com.br/green-building/331/libercon-conclui-as-obras-do-edificio-atrium-faria-lima">Veja mais informações sobre a edificação! Clique aqui!</a></font></p>', '2015-11-10 19:02:46', '2015-11-10 19:02:46'),
(6, '2015-11-10', 'O Centro de Convivência UNILEVER - FEEL GOOD recebe certificação LEED Silver!', 'o-centro-de-convivencia-unilever-feel-good-recebe-certificacao-leed-silver', '<span style="font-family: Arial; font-size: small;"><a href="http://www.revistafatorbrasil.com.br/ver_noticia.php?not=244333">Clique aqui e veja a reportagem!</a></span>', '2015-11-10 19:02:57', '2015-11-10 19:02:57'),
(7, '2015-11-10', 'Edifício Mariano Torres Corporate 729 é LEED Gold!', 'edificio-mariano-torres-corporate-729-e-leed-gold', '<span style="font-family: Arial; font-size: small; background-color: rgb(255, 255, 255);">A edificação recebeu a Certificação LEED for Core &amp; Sheel GOLD!&nbsp;</span><div><span style="font-family: Arial; font-size: small; background-color: rgb(255, 255, 255);"><br></span></div><div><span style="font-family: Arial; font-size: small; background-color: rgb(255, 255, 255);"><a href="http://www.marianotorres729.com.br/sustentabilidade.php">Clique aqui e veja mais detalhes sobre o projeto.</a></span></div><div><br></div>', '2015-11-10 19:03:06', '2015-11-10 19:03:06'),
(8, '2015-11-10', 'Planejamento: a base da eficiência energética!', 'planejamento-a-base-da-eficiencia-energetica', 'Em entrevista ao Green Building Council Brasil falamos sobre a importância do Planejamento nos projetos que buscam Eficiência Energética! <a href="http://blog.gbcbrasil.org.br/?p=186">Clique aqui.</a>', '2015-11-10 19:03:39', '2015-11-10 19:03:39'),
(9, '2015-11-10', 'O Armazém B. Braun Brasil Guaxindiba/RJ recebeu Certificação LEED GOLD!!! Agradecemos a oportunidade de participar de mais este projeto de sucesso!!!', 'o-armazem-b-braun-brasil-guaxindiba-rj-recebeu-certificacao-leed-gold-agradecemos-a-oportunidade-de-participar-de-mais-este-projeto-de-sucesso', '<div><font face="Arial"><a href="http://www.bbraun.com.br/cps/rde/xchg/cw-bbraun-pt-br/hs.xsl/10640.html">Clique aqui</a> para mais informações sobre o projeto!</font></div>', '2015-11-10 19:03:57', '2015-11-10 19:03:57'),
(10, '2015-11-10', 'CURSOS DE CAPACITAÇÃO E DESENVOLVIMENTO!!! Em breve divulgaremos DATAS!!!', 'cursos-de-capacitacao-e-desenvolvimento-em-breve-divulgaremos-datas', '<span style="line-height: 22px; font-family: Helvetica, Arial; color: rgb(134, 134, 134); font-size: 13px; background-color: rgb(238, 238, 238);"><font color="#ffffff"><font face="Arial Black"><font style="background-color: rgb(155, 187, 89);"><font size="5">&nbsp;<span style="font-size: 10pt;">EM BREVE!!!</span></font><span style="font-size: 10pt;"><font size="4"></font></span></font><font size="4" style="background-color: rgb(255, 255, 255);"><span style="font-size: 10pt;"></span></font></font></font></span><div></div><span style="line-height: 22px; font-family: Helvetica, Arial; color: rgb(134, 134, 134); font-size: 13px; background-color: rgb(238, 238, 238);"></span><div align="center"><div></div><div align="left"><font color="#4f6228" size="4" face="Arial Black"><strong><br></strong></font></div><div align="left"><font color="#4f6228" size="4" face="Arial Black"><strong>CURSOS DE:</strong></font></div><div align="left"><font size="4" face="Arial"></font></div><div align="left"><font color="#4f6228"><strong><font face="Arial"><font size="4"><span class="Apple-tab-span" style="white-space: pre;">	</span>&nbsp;&nbsp;</font></font></strong></font></div><div align="left"><font color="#4f6228"><strong><font face="Arial"><font size="4">Simulação Computacional Termoenergética -&nbsp;</font></font></strong></font><font color="#4f6228"><strong><font face="Arial"><font size="4">Software EnergyPlus</font></font></strong></font></div><div align="left"><em style="color: rgb(79, 98, 40); font-family: Arial; font-size: large;">Módulos Básico e Avançado</em></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial; font-size: large;"><br></strong></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial; font-size: large;">Eficiência Energética e Conforto em Edificações</strong></div><div align="left"><font color="#4f6228"><font face="Arial"><font size="4"><em>Conceitos Básicos, Normas Técnicas,&nbsp;</em></font></font></font><font color="#4f6228"><em><font face="Arial"><font size="4">Certificações de Edificações Sustentáveis</font></font></em></font></div><div align="left"><strong style="color: rgb(79, 98, 40);"><font face="Arial"><font size="4"><br></font></font></strong></div><div align="left"><strong style="color: rgb(79, 98, 40);"><font face="Arial"><font size="4">Norma de Desempenho NBR 15575</font></font></strong></div><div align="left"><em style="font-size: large; color: rgb(79, 98, 40); font-family: Arial;">Desempenho térmico e lumínico por meio de simulações</em></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial;"><em><font style="font-size: 12pt;"><br></font></em></strong></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial;"><em><font style="font-size: 12pt;"><br></font></em></strong></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial;"><em><font style="font-size: 12pt;">Envie-nos um email e receba mais informações!</font></em></strong></div><span style="text-align: left; font-size: 18pt;"><p align="left" style="margin-top: 0pt; unicode-bidi: embed; direction: ltr; margin-bottom: 0pt; margin-left: 0.5in; vertical-align: middle; word-break: normal;"></p><div><strong style="color: rgb(79, 98, 40); font-size: 18pt;"><span style="font-size: 14pt;"><font color="#4f6228" size="3" face="Arial"><strong><span style="color: rgb(255, 102, 0);"><em>Consulte-nos também sobre cursos personalizados para sua empresa!</em></span></strong></font></span></strong></div><div><em style="font-size: 18pt;"><b><span style="font-size:18.0pt;font-family:&quot;Arial Black&quot;,&quot;sans-serif&quot;;\\r\\nmso-fareast-font-family:Verdana;mso-bidi-font-family:Arial;color:#666633;\\r\\nmso-font-kerning:12.0pt;text-shadow:auto"><br></span></b></em></div><div style="text-align: center;"><em style="font-size: 18pt;"><b><span style="font-size:18.0pt;font-family:&quot;Arial Black&quot;,&quot;sans-serif&quot;;\\r\\nmso-fareast-font-family:Verdana;mso-bidi-font-family:Arial;color:#666633;\\r\\nmso-font-kerning:12.0pt;text-shadow:auto"><a href="http://www.espacosustentavel.com/contato">cursos@espacosustentavel.com</a></span></b></em></div></span><p class="MsoNormal"><span style="font-family:" arial="" black","sans-serif";color:#666633"=""><o:p></o:p></span></p><span style="text-align: left; font-size: 18pt;"><p class="MsoNormal"><span style="font-family:" arial="" black","sans-serif";color:#666633"=""><o:p></o:p></span></p></span><p class="MsoNormal"><span style="font-family:" arial="" black","sans-serif";color:#666633"=""><o:p></o:p></span></p><p class="MsoNormal" align="center"></p><div class="MsoNormal" align="left" style="text-align: left;"><span style="font-size: 12pt;"><hr size="2" width="100%" align="left"></span></div><span style="text-align: left; font-size: 18pt;"><p></p><p align="left" style="margin-top: 0pt; unicode-bidi: embed; direction: ltr; margin-bottom: 0pt; margin-left: 0.5in; vertical-align: middle; word-break: normal;"></p><div dir="ltr" style="font-family: Calibri; font-size: 12pt;"><div align="left"><strong><em><font color="#808040" face="Arial">Aumente a eficiência energética e o conforto de seus projetos! Edificações novas ou existentes!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Faça análises de consumo de energia e payback de sistemas de ar-condicionado e iluminação!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Escolha os materiais de construção mais adequados para que sua edificação seja confortável e eficiente energeticamente!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Avalie os pré-requisitos de energia e conforto para obter uma certificação de edificações sustentáveis: Procel, LEED, AQUA-HQE.</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Desenvolva o know-how para gestão sustentável de empreendimentos imobiliários nas etapas de pré-projeto e projeto, com embasamento técnico e visão crítica no que tange à eficiência energética da edificação e conforto ambiental.</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Entenda o impacto no consumo de energia relacionado à qualidade dos projetos de arquitetura, sistemas de ar-condicionado e iluminação!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Desenvolva as simulações computacionais necessárias para o atendimento aos requisitos de desempenho térmico e lumínico da Norma de Desempenho de Edificações Habitacionais NBR 15575-2013! Apresente melhores resultados e atenda à Norma!</font></em></strong></div><div><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div></div></span></div>', '2015-11-10 19:04:05', '2015-11-10 19:04:05');

-- --------------------------------------------------------

--
-- Table structure for table `noticias_arquivos`
--

CREATE TABLE IF NOT EXISTS `noticias_arquivos` (
  `id` int(10) unsigned NOT NULL,
  `noticia_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$QOzKR88PHS2gl6yeZkEfGO0mA/wm7RRNvr57T0jeL197ifybsJyDi', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cursos_arquivos`
--
ALTER TABLE `cursos_arquivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cursos_arquivos_curso_id_foreign` (`curso_id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias_arquivos`
--
ALTER TABLE `noticias_arquivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `noticias_arquivos_noticia_id_foreign` (`noticia_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cursos_arquivos`
--
ALTER TABLE `cursos_arquivos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `noticias_arquivos`
--
ALTER TABLE `noticias_arquivos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cursos_arquivos`
--
ALTER TABLE `cursos_arquivos`
  ADD CONSTRAINT `cursos_arquivos_curso_id_foreign` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `noticias_arquivos`
--
ALTER TABLE `noticias_arquivos`
  ADD CONSTRAINT `noticias_arquivos_noticia_id_foreign` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
