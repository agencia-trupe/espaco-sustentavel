<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:: Espaço Sustentável</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.id; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' deve conter um endereço de e-mail válido.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += ''+nm+'\n'; }
    } if (errors) alert('Os seguintes campos devem ser preenchidos:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
//-->
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td colspan="2"><? include ("inc_topo.php");?></td></tr>
      <tr>
        <td width="240" valign="top"><? include ("inc_menu.php");?></td>
        <td width="500" valign="top"><form id="form1" name="form1" method="post" action="sendmail.php">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="65" class="pad5">Nome:</td>
              <td><label>
                <input name="nome" type="text" class="imput" id="Nome"/>
              </label></td>
            </tr>
            <tr>
              <td class="pad5">Empresa:</td>
              <td><input name="empresa" type="text" class="imput" id="empresa" /></td>
            </tr>
            <tr>
              <td class="pad5">E-mail:</td>
              <td><input name="email" type="text" class="imput" id="E-mail" /></td>
            </tr>
            <tr>
              <td class="pad5">Telefone:</td>
              <td>(
                <input name="ddd" type="text" class="imput2" id="ddd" maxlength="2" />
                )
                <input name="telefone" type="text" class="imput3" id="telefone" maxlength="8" /></td>
            </tr>
            <tr>
              <td valign="top" class="pad5">Mensagem:</td>
              <td><label>
                <textarea name="mensagem" cols="45" rows="5" class="imput4" id="Mensagem"></textarea>
              </label></td>
            </tr>
            <tr>
              <td align="right" class="pad5">&nbsp;</td>
              <td align="right"><label>
                <input name="button" type="submit" class="bt1" id="button" value="Enviar" onclick="MM_validateForm('nome','','R','email','','RisEmail','mensagem','','R');return document.MM_returnValue"  />
              </label></td>
            </tr>
          </table>
        </form>          <p class="pad5">Av. Lacerda Franco, 570, s.104<br />
          01536-000 •São Paulo - SP • Brasil<br />
          <a href="mailto:espacosustentavel@espacosustentavel.com" class="link_texto2">espacosustentavel@espacosustentavel.com</a></p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><? include ("inc_rodape.php");?></td>
  </tr>
</table>
<? include ("inc_restart.php");?>
</body>
</html>