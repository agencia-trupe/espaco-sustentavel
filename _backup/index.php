<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:: Espaço Sustentável</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td colspan="2"><? include ("inc_topo.php");?></td></tr>
      <tr>
        <td width="240" valign="top"><? include ("inc_menu.php");?></td>
        <td width="500" valign="top"><p><img src="_img/arvore.jpg" width="500" height="129" /></p>
          <p>A<span class="tx_14"> Espaço Sustentável</span> é uma empresa de consultoria criada para auxiliar seus clientes a implantar ou aprimorar a eficiência energética em suas edificações, conciliando seus projetos aos conceitos de desenvolvimento sustentável.</p>
          <p>Auxiliamos projetistas e empreendedores na tomada de decisões, tendo como objetivo a redução do consumo e custo com energia, buscando sempre as melhores soluções técnicas, ambientais e econômicas para edificações novas ou existentes (retrofits).</p>
          <p>Nossa experiência abrange obras de edifícios comerciais e residenciais, shopping centers, bancos, fábricas, galpões, centros de distribuição, restaurantes, fast-foods e hospitais.</p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><? include ("inc_rodape.php");?></td>
  </tr>
</table>
<? include ("inc_restart.php");?>
</body>
</html>