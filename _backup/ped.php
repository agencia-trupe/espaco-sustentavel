<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:: Espaço Sustentável</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td colspan="2"><? include ("inc_topo.php");?></td></tr>
      <tr>
        <td width="240" valign="top"><? include ("inc_menu.php");?></td>
        <td width="500" valign="top"><p><span class="tx_12">Dissertação:</span><br />
INATOMI, THAIS A. H..<strong><a href="pdf/INATOMI_TAH_MESTRADO_2008.pdf" target="_blank" class="link_texto"> Análise  da eficiência energética do sistema de condicionamento de ar com distribuição  pelo piso em ambiente de escritório, na cidade de São Paulo, utilizando o  modelo computacional EnergyPlus.</a></strong> São Paulo,2008. 102 f. Dissertação  (Mestrado) - Escola Politécnica da Universidade de São Paulo, EPUSP, 2008.</p>
          <p><span class="tx_12">Boletim Técnico:</span><br />
            INATOMI, THAIS A. H.. <strong><a href="pdf/INATOMI_TAH_BOLETIM.pdf" target="_blank" class="link_texto">Análise  da eficiência energética do sistema de condicionamento de ar com distribuição  pelo piso em ambiente de escritório, na cidade de São Paulo, utilizando o  modelo computacional EnergyPlus.</a></strong>São Paulo, 2008. 27 p. Boletim Técnico da  Escola Politécnica da USP, Departamento de Engenharia de Construção Civil;  BT/PCC/511. São Paulo, EPUSP, 2008.<strong></strong></p>
          <p>            <span class="tx_12">Artigos Técnicos:</span><br />
            INATOMI, T. A. H. ; MOTEZUKI, F. K.; LEITE, B. C. C.; CHENG,  L.Y.<a href="pdf/INATOMI_TAHI_encac2007.pdf" target="_blank" class="link_texto"> <strong>Estudo de uma Estratégia para  Controle Fuzzy de Ambientes Condicionados com o Sistema UFAD.</strong></a> In: ENCAC  2007 - IX Encontro Nacional e V Latino Americano de Conforto no Ambiente  Construído, 2007, Ouro Preto. Anais do IX Encontro Nacional e V Latino  Americano de Conforto no Ambiente Construído ENCAC - 8-10 Agosto 2007.</p>
          <p>INATOMI, T. A. H. ; ABE, V. C. ; LEITE, B. C. C. .<a href="pdf/INATOMI_TAHI_PLEA2006.pdf" target="_blank" class="link_texto"> <strong>Energy Consumption of Underfloor Air Distribution Systems: A Literature  Overview.</strong> </a>In:  Passive and Low Energy Architecture, 2006, Geneva. Passive and Low Energy  Architecture 2006 - 6-8 September. </p>
INATOMI, T. A. H.; UDAETA, M.  E. M.<a href="pdf/INATOMI_TAHI_IMPACTOS_AMBIENTAIS.pdf" target="_blank" class="link_texto"> <strong>Análise dos Impactos Ambientais na  Produção de Energia dentro do Planejamento Integrado de Recursos.</strong></a> In: III  Workshop Internacional Brasil - Japão: Implicações Regionais e Globais em  Energia, Meio Ambiente e Desenvolvimento Sustentável, 2005, Campinas - Brasil.  Anais do III Workshop Internacional Brasil - Japão: Implicações Regionais e  Globais em Energia, Meio Ambiente e Desenvolvimento Sustentável, 2005.</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><? include ("inc_rodape.php");?></td>
  </tr>
</table>
<? include ("inc_restart.php");?>
</body>
</html>