<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:: Espaço Sustentável</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td colspan="2"><? include ("inc_topo.php");?></td></tr>
      <tr>
        <td width="240" valign="top"><? include ("inc_menu.php");?></td>
        <td width="500" valign="top"><p><span class="tx_12">Consultoria para minimizar o consumo de energia relacionado a projetos de: </span><br />
          •	Arquitetura<br />
          •	Envoltória (especificação de vidros e materiais de construção)<br />
          •	Sistemas de condicionamento de ar e Ventilação<br />
          •	Luminotécnica<br />
          •	Automação<br />
          •	Aquecimento de água</p>
          <p><span class="tx_12">Análise de viabilidade de implantação de sistemas de:</span><br />
            •	Iluminação natural<br />
            •	Ventilação Natural<br />
            •	Sistemas híbridos de ventilação natural e ar condicionado<br />
            •	Energia renovável</p>
          <p><span class="tx_12">Análise tarifária de energia</span><br />
            Simulação computacional do desempenho térmico e energético de edificações utilizando os softwares EnergyPlus, Window5, Optics, entre outros.</p>
          <p> Consultoria e simulação energética para certificações LEED (Leadership in Energy and Environmental Design) <br />
          </p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><? include ("inc_rodape.php");?></td>
  </tr>
</table>
<? include ("inc_restart.php");?>
</body>
</html>