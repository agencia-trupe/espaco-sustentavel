-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 201.76.55.136
-- Tempo de Geração: 10/11/2015 às 10:21:55
-- Versão do Servidor: 5.1.54
-- Versão do PHP: 5.3.3-7+squeeze27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `espacosustenta`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `texto` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `texto`) VALUES
(2, 'Curitiba Office Park é LEED Silver!', '<p><span style="font-family: Arial; font-size: small;">Empreendimento atinge 19% de redução do consumo de energia em relação à ASHRAE 90.1-2004</span></p><p><font face="Arial" size="2">[url=www.gazetadopovo.com.br/imobiliario/conteudo.phtml?id=1161386&amp;tit=Curitiba-entra-na-era-dos-edificios-sustentaveis]Clique aqui e veja mais informações.[/url]</font>&nbsp;</p>'),
(4, 'O Novo Auditório do Edifício Sede da Odebrecht recebe Certificação LEED Silver!', '<p></p><p class="MsoNormal"></p><p class="MsoNormal"></p><p class="MsoNormal"><font size="2" face="Arial">A edificação alcançou eficiência energética de 21% em relação ao edifício de referência estabelecido pela norma ASHRAE 90.1 - 2004!</font></p>'),
(5, 'Centro de Distribuição da Iveco e Case New Holland recebe Certificação LEED Gold! ', '<span style="font-family: Arial; font-size: small;">O&nbsp;empreendimento atingiu eficiência energética de 46% e 40% nos seus dois galpões, conforme estabelecido pela norma ASHRAE 90.1-2004.</span><div><span style="font-family: Arial; font-size: small;"><br></span></div><div><span style="font-family: Arial; font-size: small;">[url=www.ivecodiretto.com.br/PAGINA/620/CENTRO-DE-DISTRIBUI--199---195-O-DA-IVECO-E-CASE-NEW-HOLLAND-RECEBE-CERTIFICA--199---195-O-GREEN-BUILDING.aspx]Clique e veja mais informações.[/url]&nbsp;</span></div>'),
(6, 'Unidade Alphaville do Fleury Medicina e Saúde recebe certificação LEED Gold!', '<p class="MsoNormal" style="margin-bottom: 0.0001pt;"></p><p class="MsoNormal" style="margin-bottom: 0.0001pt;"><font face="Arial" size="2">[url=www.revistainfra.com.br/portal/Textos/?Slider/13383/Fleury-Alphaville-%C3%A9-LEED-Ouro-]Veja a reportagem! Clique aqui.[/url]</font></p>'),
(7, 'Edifício Atrium Faria Lima recebe Certificação LEED!', '<p><font face="Arial">[url=www.construirsustentavel.com.br/green-building/331/libercon-conclui-as-obras-do-edificio-atrium-faria-lima]Veja mais informações sobre a edificação! Clique aqui![/url]</font></p>'),
(8, 'O Centro de Convivência UNILEVER - FEEL GOOD recebe certificação LEED Silver!', '<span style="font-family: Arial; font-size: small;">[url=www.revistafatorbrasil.com.br/ver_noticia.php?not=244333]Clique aqui e veja a reportagem![/url]</span>'),
(9, 'Edifício Mariano Torres Corporate 729 é LEED Gold!', '<span style="font-family: Arial; font-size: small; background-color: rgb(255, 255, 255);">A edificação recebeu a Certificação LEED for Core &amp; Sheel GOLD!&nbsp;</span><div><span style="font-family: Arial; font-size: small; background-color: rgb(255, 255, 255);"><br></span></div><div><span style="font-family: Arial; font-size: small; background-color: rgb(255, 255, 255);">[url=www.marianotorres729.com.br/sustentabilidade.php]Clique aqui e veja mais detalhes sobre o projeto.[/url]</span></div><div><br></div>'),
(11, 'Planejamento: a base da eficiência energética! ', 'Em entrevista ao Green Building Council Brasil falamos sobre a importância do Planejamento nos projetos que buscam Eficiência Energética! [url=blog.gbcbrasil.org.br/?p=186]Clique aqui.[/url]'),
(14, 'O Armazém B. Braun Brasil Guaxindiba/RJ recebeu Certificação LEED GOLD!!! Agradecemos a oportunidade de participar de mais este projeto de sucesso!!!', '<div><font face="Arial">[url=www.bbraun.com.br/cps/rde/xchg/cw-bbraun-pt-br/hs.xsl/10640.html]Clique aqui[/url] para mais informações sobre o projeto!</font></div>'),
(15, 'CURSOS DE CAPACITAÇÃO E DESENVOLVIMENTO!!! Em breve divulgaremos DATAS!!!', '<span style="line-height: 22px; font-family: Helvetica, Arial; color: rgb(134, 134, 134); font-size: 13px; background-color: rgb(238, 238, 238);"><font color="#ffffff"><font face="Arial Black"><font style="background-color: rgb(155, 187, 89);"><font size="5">&nbsp;<span style="font-size: 10pt;">EM BREVE!!!</span></font><span style="font-size: 10pt;"><font size="4"></font></span></font><font size="4" style="background-color: rgb(255, 255, 255);"><span style="font-size: 10pt;"></span></font></font></font></span><div></div><span style="line-height: 22px; font-family: Helvetica, Arial; color: rgb(134, 134, 134); font-size: 13px; background-color: rgb(238, 238, 238);"></span><div align="center"><div></div><div align="left"><font color="#4f6228" size="4" face="Arial Black"><strong><br></strong></font></div><div align="left"><font color="#4f6228" size="4" face="Arial Black"><strong>CURSOS DE:</strong></font></div><div align="left"><font size="4" face="Arial"></font></div><div align="left"><font color="#4f6228"><strong><font face="Arial"><font size="4"><span class="Apple-tab-span" style="white-space: pre;">	</span>&nbsp;&nbsp;</font></font></strong></font></div><div align="left"><font color="#4f6228"><strong><font face="Arial"><font size="4">Simulação Computacional Termoenergética -&nbsp;</font></font></strong></font><font color="#4f6228"><strong><font face="Arial"><font size="4">Software EnergyPlus</font></font></strong></font></div><div align="left"><em style="color: rgb(79, 98, 40); font-family: Arial; font-size: large;">Módulos Básico e Avançado</em></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial; font-size: large;"><br></strong></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial; font-size: large;">Eficiência Energética e Conforto em Edificações</strong></div><div align="left"><font color="#4f6228"><font face="Arial"><font size="4"><em>Conceitos Básicos, Normas Técnicas,&nbsp;</em></font></font></font><font color="#4f6228"><em><font face="Arial"><font size="4">Certificações de Edificações Sustentáveis</font></font></em></font></div><div align="left"><strong style="color: rgb(79, 98, 40);"><font face="Arial"><font size="4"><br></font></font></strong></div><div align="left"><strong style="color: rgb(79, 98, 40);"><font face="Arial"><font size="4">Norma de Desempenho NBR 15575</font></font></strong></div><div align="left"><em style="font-size: large; color: rgb(79, 98, 40); font-family: Arial;">Desempenho térmico e lumínico por meio de simulações</em></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial;"><em><font style="font-size: 12pt;"><br></font></em></strong></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial;"><em><font style="font-size: 12pt;"><br></font></em></strong></div><div align="left"><strong style="color: rgb(79, 98, 40); font-family: Arial;"><em><font style="font-size: 12pt;">Envie-nos um email e receba mais informações!</font></em></strong></div><span style="text-align: left; font-size: 18pt;"><p align="left" style="margin-top: 0pt; unicode-bidi: embed; direction: ltr; margin-bottom: 0pt; margin-left: 0.5in; vertical-align: middle; word-break: normal;"></p><div><strong style="color: rgb(79, 98, 40); font-size: 18pt;"><span style="font-size: 14pt;"><font color="#4f6228" size="3" face="Arial"><strong><span style="color: rgb(255, 102, 0);"><em>Consulte-nos também sobre cursos personalizados para sua empresa!</em></span></strong></font></span></strong></div><div><em style="font-size: 18pt;"><b><span style="font-size:18.0pt;font-family:&quot;Arial Black&quot;,&quot;sans-serif&quot;;\r\nmso-fareast-font-family:Verdana;mso-bidi-font-family:Arial;color:#666633;\r\nmso-font-kerning:12.0pt;text-shadow:auto"><br></span></b></em></div><div style="text-align: center;"><em style="font-size: 18pt;"><b><span style="font-size:18.0pt;font-family:&quot;Arial Black&quot;,&quot;sans-serif&quot;;\r\nmso-fareast-font-family:Verdana;mso-bidi-font-family:Arial;color:#666633;\r\nmso-font-kerning:12.0pt;text-shadow:auto">[url=www.espacosustentavel.com/contato.php]cursos@espacosustentavel.COM[/url]</span></b></em></div></span><p class="MsoNormal"><span style="font-family:" arial="" black","sans-serif";color:#666633"=""><o:p></o:p></span></p><span style="text-align: left; font-size: 18pt;"><p class="MsoNormal"><span style="font-family:" arial="" black","sans-serif";color:#666633"=""><o:p></o:p></span></p></span><p class="MsoNormal"><span style="font-family:" arial="" black","sans-serif";color:#666633"=""><o:p></o:p></span></p><p class="MsoNormal" align="center"></p><div class="MsoNormal" align="left" style="text-align: left;"><span style="font-size: 12pt;"><hr size="2" width="100%" align="left"></span></div><span style="text-align: left; font-size: 18pt;"><p></p><p align="left" style="margin-top: 0pt; unicode-bidi: embed; direction: ltr; margin-bottom: 0pt; margin-left: 0.5in; vertical-align: middle; word-break: normal;"></p><div dir="ltr" style="font-family: Calibri; font-size: 12pt;"><div align="left"><strong><em><font color="#808040" face="Arial">Aumente a eficiência energética e o conforto de seus projetos! Edificações novas ou existentes!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Faça análises de consumo de energia e payback de sistemas de ar-condicionado e iluminação!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Escolha os materiais de construção mais adequados para que sua edificação seja confortável e eficiente energeticamente!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Avalie os pré-requisitos de energia e conforto para obter uma certificação de edificações sustentáveis: Procel, LEED, AQUA-HQE.</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Desenvolva o know-how para gestão sustentável de empreendimentos imobiliários nas etapas de pré-projeto e projeto, com embasamento técnico e visão crítica no que tange à eficiência energética da edificação e conforto ambiental.</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Entenda o impacto no consumo de energia relacionado à qualidade dos projetos de arquitetura, sistemas de ar-condicionado e iluminação!</font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial"></font></em></strong></div><div align="left"><strong><em><font color="#808040" face="Arial">Desenvolva as simulações computacionais necessárias para o atendimento aos requisitos de desempenho térmico e lumínico da Norma de Desempenho de Edificações Habitacionais NBR 15575-2013! Apresente melhores resultados e atenda à Norma!</font></em></strong></div><div><strong><em><font color="#808040" face="Arial"><br></font></em></strong></div></div></span></div>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `senha` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `login`, `senha`) VALUES
(1, 'admin', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
